<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib uri = "http://java.sun.com/jsp/jstl/core" prefix = "c" %>    
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Insert title here</title>
</head>
<body>
	<h3 align="center">--------------------------------------------------------------------------------------------------------------------------------</h3>
	<h1 align="center">List Of Students</h1>	
	<h2 style = "color:red;">${requestScope.mesg}</h2>
		
		<table cellpadding="10rem" cellspacing="2rem" border="1" align="center">
			<thead>
				<tr style = "color: olive; text-decoration: underline;">

					<th>Student Id</th>
					<th>Student First Name</th>
					<th>Student Last Name</th>
					<th>Student Address</th>
					<th>Selected Course</th>
					<th colspan = "2">Options</th>
				</tr>
			</thead>

			<tbody>

				<c:forEach var="slist" items="${studentList}" varStatus="status">
					<tr>

						<td>${slist.sid}</td>
						<td>${slist.fname}</td>
						<td>${slist.lname}</td>
						<td>${slist.address}</td>
						<td>${slist.course}</td>
						<td><a href = "delete?id=${slist.sid}" 
						onclick="if(!confirm('Are you sure to delete the user?')) return false">Delete Student</a></td>
						<td><a href = "update?id=${slist.sid}">Update Student details</a></td>
				
					</tr>
				</c:forEach>


			</tbody>
		</table>
		
		<h3><a href = "index">Back to Home</a></h3>
</body>
</html>