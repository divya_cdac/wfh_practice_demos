<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    <%@ taglib uri = "http://java.sun.com/jsp/jstl/core" prefix = "c" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Registration Page</title>
</head>
<body>
	<%-- <h3>${message}</h3> --%>
	<div>
		<h1>Register Page</h1>
		<form action="/register" method="post" >

		
			<input placeholder="Student Id"
				name="sid" type="text" required="required" />	
				
			<input placeholder="Password"
				name="password" type="password" required="required" />		

			<input placeholder="Student First Name"
				name="fname" type="text"
				required="required" />

			<input placeholder="Student Last Name"
				name="lname" type="text" required="required" />
				
			<input placeholder="Address"
				name="address" type="text" required="required" /> 
				
			<select name="course">
				<c:forEach items="${listCourse}" var="course">
					<option value="${course.name}">${course.name}</option>
				</c:forEach>
			</select>


			<button type="submit">Register</button>
		</form>
	</div>
</body>
</html>