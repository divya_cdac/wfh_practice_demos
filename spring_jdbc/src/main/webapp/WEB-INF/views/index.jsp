<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Index Page</title>
</head>
<body>
	<h1 align="center">Welcome to Spring JDBC Demo Application</h1>
	<h1 align="center">Basic Student Portal Application</h1>
	
	<h2 style = "color:red;">${requestScope.mesg}</h2>
	
	<h2><a href="login">Login Page</a></h2>
	<h2><a href="register">New Student? Register Here</a></h2>
	
	<h2><a href = "listStudents">List of Students</a></h2>
</body>
</html>