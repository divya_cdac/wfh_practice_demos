<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib uri = "http://java.sun.com/jsp/jstl/core" prefix = "c" %>    
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Update Details Page</title>
</head>

<body>
	<h2 style = "color:red;">${requestScope.mesg}</h2>
	<div>
		<h1>Update Page</h1>
		<form action="/update?id=${param.id}" method="post" >

			Student Id: <input value="${st.sid}"
				name="sid" type="text"
				required="required" /><br/>

			Password : <input value="${st.password}"
				name="password" type="text"
				required="required" /><br/>

			First Name: <input value="${st.fname}"
				name="fname" type="text"
				required="required" /><br/>
				
			Last Name: <input value="${st.lname}"
				name="lname" type="text"
				required="required" /><br/>
				
			Address: <input value="${st.address}"
				name="address" type="text"
				required="required" /><br/>
				
			Course: <select name="course">
				<c:forEach items="${listCourse}" var="course">
					<option value="${course.name}">${course.name}</option>
				</c:forEach>
			</select><br/><br/>	
				
			<button type="submit">Update New Details</button>
			
		</form>
	</div>
</body>

</html>