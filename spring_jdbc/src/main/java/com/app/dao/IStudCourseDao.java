package com.app.dao;

import java.util.List;
import com.app.pojos.Course;
import com.app.pojos.Student;
public interface IStudCourseDao {
	
	public List<Course> listCourses();

	public String registerStudent(Student st);

	public List<Student> listStudents();

	public String deleteStudent(String id);

	public Student getStudent(String id);

	public String updateStudent(Student st, String id);

	public String authenticate(Student st);

}
