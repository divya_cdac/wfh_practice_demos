package com.app.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;
import com.app.pojos.Course;
import com.app.pojos.Student;

@Repository
public class StudCourseDaoImpl implements IStudCourseDao {

	@Autowired
	private JdbcTemplate jdbcTemplate;
	
	@Override
	public List<Course> listCourses() {
		
		final String Load_All_Records = "SELECT * FROM public.course order by cid asc";
		
		List<Course> listCourse = jdbcTemplate.query(Load_All_Records, new RowMapper<Course>() 
		{
					 	@Override
						public Course mapRow(ResultSet rs, int rowNum) throws SQLException 
					 	{
					 		Course list=new Course();
					 		list.setCid(rs.getString("cid"));
					 		list.setName(rs.getString("name"));
					 		
						 return list;
					 }
				 }); 
				return listCourse;
	}

	@Override
	public String registerStudent(Student st) {
		
		try {
			
			String sql = "INSERT INTO student(sid,password,fname,lname,address,course) VALUES (?, ?, ?, ?, ?, ?)";
			jdbcTemplate.update(sql, st.getSid(), st.getPassword(), st.getFname(), st.getLname(), st.getAddress(), st.getCourse());
			return "New Student "+st.getFname()+" has been registered";
		}
		catch(Exception e) 
		{	e.printStackTrace();
			return "New Student "+st.getFname()+"'s registration failed. ";	
		}
	}
	
	
	

	@Override
	public Student getStudent(String id) {
		
		String sql = "SELECT * FROM student WHERE sid = ?";

		Student st =  jdbcTemplate.queryForObject(sql, new Object[]{id}, (rs, rowNum) ->
                new Student(
                		rs.getString("sid"),
                		rs.getString("password"),
				 		rs.getString("fname"),
				 		rs.getString("lname"),
				 		rs.getString("address"),
				 		rs.getString("course")
                ));
        
         return st;
	}

	@Override
	public List<Student> listStudents() {
		
		final String Load_All_Records = "SELECT * FROM public.student order by sid asc";
		
		List<Student> listStudents = jdbcTemplate.query(Load_All_Records, new RowMapper<Student>() 
		{
					 	@Override
						public Student mapRow(ResultSet rs, int rowNum) throws SQLException 
					 	{
					 		Student list=new Student();
					 		list.setSid(rs.getString("sid"));
					 		list.setFname(rs.getString("fname"));
					 		list.setLname(rs.getString("lname"));
					 		list.setAddress(rs.getString("address"));
					 		list.setCourse(rs.getString("course"));
					 		
						 return list;
					 }
				 }); 
				return listStudents;
	}

	@Override
	public String deleteStudent(String id) {
		
		String message = "Student with id: "+ id +" could not be deleted";
		try {
			String sql = "DELETE FROM student where sid = ?";
			jdbcTemplate.update(sql,id);
			message = "Student with id "+ id +" successfully deleted";
			return message;
			
		}
		
		catch(Exception e) 
		{	e.printStackTrace();
			return message;	
		}
	}

	@Override
	public String updateStudent(Student st, String id) {
		
		try {
			
			String sql = "UPDATE student set sid =?, password = ?, fname = ?, lname = ?, address = ?, course = ? where sid = ?";
			int n = jdbcTemplate.update(sql, st.getSid(),st.getPassword(),st.getFname(),st.getLname(),
					st.getAddress(),st.getCourse(),id);
			System.out.println(n);
			if(n!=0)
			{
				return "Student "+st.getFname()+ " "+st.getLname()+"'s details are successfully updated";
			}
			
			return "Student "+st.getFname()+ " "+st.getLname()+"'s details are NOT updated";
		}
		catch(Exception e) 
		{	
			e.printStackTrace();
			return "Student "+st.getFname()+ " "+st.getLname()+"'s details are NOT updated";
		}
	}

	@Override
	public String authenticate(Student st) {
		
		try {
		String sql = "SELECT * FROM student WHERE sid = ? and password = ?";

		Student std =  jdbcTemplate.queryForObject(sql, new Object[]{st.getSid(),st.getPassword()}, (rs, rowNum) ->
                new Student(
                		rs.getString("sid"),
                		rs.getString("password"),
				 		rs.getString("fname"),
				 		rs.getString("lname"),
				 		rs.getString("address"),
				 		rs.getString("course")
                ));
        
         return "Successfully Logged in as "+ std.getFname()+" "+std.getLname();
         
		}catch (Exception e) {
			return "Login not successfull!!..Check credentials";
		} 
		
	}
	
	
	
	
	

}
