package com.app.service;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.app.dao.IStudCourseDao;
import com.app.pojos.Course;
import com.app.pojos.Student;

@Service
public class StudCourServiceImpl implements IStudCourService {

	@Autowired
	IStudCourseDao sdao;
	
	@Override
	public List<Course> listCourses() {
		List<Course> courses = sdao.listCourses();
		return courses;
	}

	@Override
	public String registerStudent(Student st) {
		
		return sdao.registerStudent(st);
	}

	

	@Override
	public Student getStudent(String id) {
		return sdao.getStudent(id);
	}

	@Override
	public List<Student> listStudents() {
		
		return sdao.listStudents();
	}

	@Override
	public String deleteStudent(String id) {
		
		return sdao.deleteStudent(id);
	}

	@Override
	public String updateStudent(Student st, String id) {
		
		return sdao.updateStudent(st,id);
	}

	@Override
	public String authenticate(Student st) {
		return sdao.authenticate(st);
	}

	
	
}
