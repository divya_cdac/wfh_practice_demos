package com.app.pojos;

import org.springframework.stereotype.Component;

@Component
public class Course {
	
	private String cid;
	private String name;
	
	
	public String getCid() {
		return cid;
	}
	public void setCid(String cid) {
		this.cid = cid;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	@Override
	public String toString() {
		return "Course [Course Id=" + cid + ", Course Name=" + name + "]";
	}
	
	
	

}
