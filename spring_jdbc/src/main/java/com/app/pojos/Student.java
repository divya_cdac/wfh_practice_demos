package com.app.pojos;

import org.springframework.stereotype.Component;

@Component
public class Student {
	
	private String sid;
	private String password;
	private String fname;
	private String lname;
	private String address;
	private String course;
	
	public Student() {
		
	}
	
	public Student(String sid, String password, String fname, String lname, String address, String course) {
		super();
		this.sid = sid;
		this.password = password;
		this.fname = fname;
		this.lname = lname;
		this.address = address;
		this.course = course;
	}
	public String getSid() {
		return sid;
	}
	public void setSid(String sid) {
		this.sid = sid;
	}
	
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getFname() {
		return fname;
	}
	public void setFname(String fname) {
		this.fname = fname;
	}
	public String getLname() {
		return lname;
	}
	public void setLname(String lname) {
		this.lname = lname;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public String getCourse() {
		return course;
	}
	public void setCourse(String course) {
		this.course = course;
	}
	
	
	@Override
	public String toString() {
		return "Student [StudentID=" + sid + ", Password=" + password + ", FirstName=" + fname + ", LastName=" + lname + ", Address="
				+ address + ", Course=" + course + "]";
	}
	
	



}
