package com.app.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import com.app.pojos.Student;
import com.app.service.IStudCourService;

@Controller
public class StudentController {
	
	@Autowired
	IStudCourService scservice;
	
	@RequestMapping("/")
	 public String index() {
        return "index";
    }
	
	@RequestMapping("/index")
	 public String indexPage() {
       return "index";
   }

	@RequestMapping(value = "/register" , method = RequestMethod.GET)
	public String registerPage(Model map) {	
		
		map.addAttribute("listCourse", scservice.listCourses());
		return "register";
	}
	
	@RequestMapping(value = "/register" , method = RequestMethod.POST)
	public String registerPage(Student st, RedirectAttributes map) {
		
		String str = scservice.registerStudent(st);
		map.addFlashAttribute("mesg",str);
		
		if(str.contains("failed")) {
			return "redirect:register";
		}
		
		return "redirect:index";
	}
	
	@RequestMapping(value = "/listStudents" , method = RequestMethod.GET)
	public String listOfStudents(Model map) {	
		map.addAttribute("studentList", scservice.listStudents());
		return "listStudents";
	}
	
	@RequestMapping("/delete")
	public String deleteStudentFromList(@RequestParam String id,RedirectAttributes map)
	{
		System.out.println("in student contlr deleteList...");
		map.addFlashAttribute("mesg",scservice.deleteStudent(id));
		return "redirect:listStudents";
	}
	
	@RequestMapping("/update")
	public String updateEmployee(@RequestParam String id,Model map) {
		
		map.addAttribute("st", scservice.getStudent(id));
		map.addAttribute("listCourse", scservice.listCourses());
		return "update";
	}
	
	@RequestMapping(value = "/update" , method = RequestMethod.POST)
	public String updatePage(Student st,RedirectAttributes map,@RequestParam String id) {
		
		String str = scservice.updateStudent(st, id);
		map.addFlashAttribute("mesg",str);
		if(str.contains("NOT"))
		{
			return "redirect:update?id="+id;
		}
			
		return "redirect:listStudents";
		
	}
	
	@RequestMapping("/login")
	public String login() {
		
		return "login";
	}
	
	
	@RequestMapping(value = "/login" , method = RequestMethod.POST)
	public String loginPage(Student st, RedirectAttributes map) {
		
		//System.out.println(emp.getEmail());
		//System.out.println(emp.getPassword());
		String str = scservice.authenticate(st);
		map.addFlashAttribute("mesg",str);
		if(str.contains("not"))
		{
			return "redirect:login";
		}
			return "redirect:index";
		
	}
	
}
