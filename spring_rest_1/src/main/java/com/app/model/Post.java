package com.app.model;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="posts")
public class Post {
	
	private Integer pid;
	private String title;
	private String body;
	
	public Post() {
		
	}

	public Post(Integer pid, String title, String body) {
		super();
		this.pid = pid;
		this.title = title;
		this.body = body;
	}

	@Id
	public Integer getPid() {
		return pid;
	}

	public void setPid(Integer pid) {
		this.pid = pid;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getBody() {
		return body;
	}

	public void setBody(String body) {
		this.body = body;
	}

	@Override
	public String toString() {
		return "Post [pid=" + pid + ", title=" + title + ", body=" + body + "]";
	}
	
	

}
