package com.app.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.app.model.Post;
import com.app.repository.PostRepository;

@Service
public class PostService {

	@Autowired
	private PostRepository prepo;
	
	public List<Post> getPosts(){
		
		List<Post> list = new ArrayList<>();
		for(Post post : prepo.findAll())
		{
			list.add(post);
		}
		
		return list;
	}
	
	
	public Post getPostById(Integer id) {
		
		return prepo.findById(id).get();
	}
	
	public void addPost(Post element) {
		
		prepo.save(element);
	}

	public void updatePost(Post post) {
		
		prepo.save(post);
		
	}

	public void deletePost(Integer id) {
		
		prepo.deleteById(id);
	}






}
