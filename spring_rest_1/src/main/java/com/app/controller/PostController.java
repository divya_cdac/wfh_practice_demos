package com.app.controller;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import com.app.model.Post;
import com.app.service.PostService;

@RestController
public class PostController {
	
	@Autowired
	private PostService pservice;
	
	@RequestMapping("/posts")
	public List<Post> getPosts(){
		
		return pservice.getPosts();
	}

	@RequestMapping("/posts/{id}")
	public Post getPostsById(@PathVariable Integer id){
		
		return pservice.getPostById(id);
	}
	
	
	@RequestMapping(method=RequestMethod.POST, value="/posts")
	public void addPost(@RequestBody Post element) {
		
		pservice.addPost(element);
	}
	
	@RequestMapping(method=RequestMethod.PUT, value="/posts/{id}")
	public void updatePost(@RequestBody Post post) {
		
		pservice.addPost(post);
	}
	
	@RequestMapping(method=RequestMethod.DELETE, value="/posts/{id}")
	public void deletePost(@PathVariable Integer id) {
		
		pservice.deletePost(id);
	}
	
	
}
